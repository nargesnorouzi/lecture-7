package edu.ucsc.cmps121.lecture7;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    public static  final String MASTER_FLAG = "Master";
    public static  final String DETAIL_FLAG = "Detail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout rootView = new LinearLayout(this);
        setContentView(rootView);

        FrameLayout masteLayout = new FrameLayout(this);
        FrameLayout detailLayout = new FrameLayout(this);

        rootView.addView(masteLayout, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        rootView.addView(detailLayout, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2));

        //View redView = new View(this);
        //redView.setBackgroundColor(Color.RED);
        //View greenView = new View(this);
        //greenView.setBackgroundColor(Color.GREEN);

        //masteLayout.addView(redView);
        //detailLayout.addView(greenView);
        //getSupportFragmentManager is for compatibility!
        masteLayout.setId(10);
        detailLayout.setId(11);

        MasterFragment masterFragment = (MasterFragment) getSupportFragmentManager().findFragmentByTag(MASTER_FLAG);
        DetailFragment detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentByTag(DETAIL_FLAG);
        if(masterFragment == null && detailFragment == null){
            masterFragment = MasterFragment.newInstance();
            detailFragment = DetailFragment.newInstance();
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(masteLayout.getId(), masterFragment, MASTER_FLAG);
        fragmentTransaction.add(detailLayout.getId(), detailFragment, DETAIL_FLAG);
        fragmentTransaction.commit();



    }
}
