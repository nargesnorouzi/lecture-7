package edu.ucsc.cmps121.lecture7;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by nargesnorouzi on 2017-10-19.
 */

public class MasterFragment extends Fragment{

    public static MasterFragment newInstance(){
        return new MasterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View blueView = new View(getActivity());
        blueView.setBackgroundColor(Color.BLUE);
        return blueView;
    }
}
