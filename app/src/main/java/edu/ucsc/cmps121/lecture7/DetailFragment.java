package edu.ucsc.cmps121.lecture7;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by nargesnorouzi on 2017-10-19.
 */

public class DetailFragment extends Fragment{
    public static DetailFragment newInstance(){
        return new DetailFragment();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View yellowView = new View(getActivity());
        yellowView.setBackgroundColor(Color.YELLOW);
        return yellowView;
    }
}
